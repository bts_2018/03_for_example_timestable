import java.util.Scanner;

public class main {

	public static void main(String[] args) {

		System.out.println("Which times table do you want?");

		Scanner user_input = new Scanner(System.in);

		int times_table = user_input.nextInt();

		for (int i = 1; i <= 10; i++) {
			System.out.println(i + " * " + times_table + " = " + i * times_table);

		}

	}

}
